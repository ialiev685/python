# -*- coding: utf-8 -*-

import simple_draw as sd

# На основе кода из lesson_004/05_snowfall.py
# сделать модуль snowfall.py в котором реализовать следующие функции
#  создать_снежинки(N) - создает N снежинок
#  нарисовать_снежинки_цветом(color) - отрисовывает все снежинки цветом color
#  сдвинуть_снежинки() - сдвигает снежинки на один шаг
#  номера_достигших_низа_экрана() - выдает список номеров снежинок, которые вышли за границу экрана
#  удалить_снежинки(номера) - удаляет снежинки с номерами из списка
# снежинки хранить в глобальных переменных модуля snowfall
#
# В текущем модуле реализовать главный цикл падения снежинок,
# обращаясь ТОЛЬКО к функциям модуля snowfall

from snowfall import create_snowflakes, draw_snowflakes, move_snowflakes, remove_snowflakes, get_fallen_snowflakes

create_snowflakes(10)
while True:
    sd.start_drawing()
    draw_snowflakes(color=sd.background_color)
    move_snowflakes()
    draw_snowflakes(color=sd.COLOR_BLUE)
    indexes_fallen_snowflake = get_fallen_snowflakes()
    if len(indexes_fallen_snowflake) > 0:
        remove_snowflakes()
    sd.finish_drawing()
    sd.sleep(0.1)
    if sd.user_want_exit():
        break
