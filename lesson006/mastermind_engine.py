from random import randint

global bulls
global cows


def check_type_string(add_value):
    try:
        if isinstance(int(add_value), int):
            return False
    except ValueError:
        return True


def make_random_number():
    number = randint(1000, 2000)
    random_number = number
    return random_number


def check_number_positionally(add_value, random_value):
    bulls = 0
    cows = 0

    len_add_value = len(str(add_value))
    len_random_value = len(str(random_value))

    modern_random_value = str(random_value)
    print('random_value', random_value)
    for index_add_value, number_add_value in enumerate(add_value[:len_add_value]):
        if modern_random_value[index_add_value] == number_add_value:
            bulls += 1
        for index_random_value, number_random_value in enumerate(modern_random_value[:len_random_value]):
            if index_random_value != index_add_value and number_add_value == number_random_value:
                cows += 1
    return {'bulls': bulls, 'cows': cows}


def check_number(add_value, random_value):
    if check_type_string(add_value):
        return 'Введенное число должно быть числом'
    if len(add_value) != 4:
        return 'Введенное число должно быть 4-х значным'

    if add_value == random_value:
        return {'bulls': 4, 'cows': 0}
    else:
        return check_number_positionally(add_value, random_value)
