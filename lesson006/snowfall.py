import simple_draw as sd

sd.resolution = (1200, 600)
snowflakes = []
step = 20
length_snowflake = 20
screen_edge = 50
fallen_snowflake = []


def create_snowflakes(count_snowflakes):
    for _ in range(0, count_snowflakes, 1):
        x = sd.random_number(100, 1000)
        y = sd.random_number(500, 600)

        snowflakes.append({'x': x, 'y': y})
    return snowflakes


def draw_snowflakes(color):
    for item in snowflakes:
        x = item['x']
        y = item['y']
        point = sd.get_point(x, y)
        sd.snowflake(center=point, length=length_snowflake, color=color)


def move_snowflakes():
    for index, item in enumerate(snowflakes):
        snowflakes[index]['y'] -= step


def remove_snowflakes():
    for item in fallen_snowflake:
        coordinate = snowflakes.pop(item)
        point = sd.get_point(coordinate['x'], coordinate['y'])
        sd.snowflake(center=point, length=length_snowflake, color=sd.background_color)
        create_new_snowflake(item)
    fallen_snowflake.clear()


def create_new_snowflake(index):
    x = sd.random_number(100, 1000)
    y = sd.random_number(500, 600)
    snowflakes.insert(index, {'x': x, 'y': y})


def get_fallen_snowflakes():
    for index, item in enumerate(snowflakes):
        if item['y'] < screen_edge:
            fallen_snowflake.append(index)
    return fallen_snowflake
