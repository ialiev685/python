# -*- coding: utf-8 -*-

import os
import time
import shutil
from zipfile import ZipFile

# Нужно написать скрипт для упорядочивания фотографий (вообще любых файлов)
# Скрипт должен разложить файлы из одной папки по годам и месяцам в другую.
# Например, так:
#   исходная папка
#       icons/cat.jpg
#       icons/man.jpg
#       icons/new_year_01.jpg
#   результирующая папка
#       icons_by_year/2018/05/cat.jpg
#       icons_by_year/2018/05/man.jpg
#       icons_by_year/2017/12/new_year_01.jpg
#
# Входные параметры основной функции: папка для сканирования, целевая папка.
# Имена файлов в процессе работы скрипта не менять, год и месяц взять из времени последней модификации файла
# (время создания файла берется по разному в разых ОС - см https://clck.ru/PBCAX - поэтому берем время модификации).
#
# Файлы для работы взять из архива icons.zip - раззиповать проводником ОС в папку icons перед написанием кода.
# Имя целевой папки - icons_by_year (тогда она не попадет в коммит, см .gitignore в папке ДЗ)
#
# Пригодятся функции:
#   os.walk
#   os.path.dirname
#   os.path.join
#   os.path.normpath
#   os.path.getmtime
#   time.gmtime
#   os.makedirs
#   shutil.copy2
#
# Чтение документации/гугла по функциям - приветствуется. Как и поиск альтернативных вариантов :)
#
# Требования к коду: он должен быть готовым к расширению функциональности - делать сразу на классах.
# Для этого пригодится шаблон проектирование "Шаблонный метод"
#   см https://refactoring.guru/ru/design-patterns/template-method
#   и https://gitlab.skillbox.ru/vadim_shandrinov/python_base_snippets/snippets/4


path_folder_zip = 'icons.zip'
path_folder_result = 'icons_by_year'


class SortFile:
    def __init__(self, file_name, result_file_name):
        self.folder_zip_name = file_name
        self.result_folder_name = result_file_name

    def create_folder(self, year, month):
        month = '0' + str(month) if month <= 9 else month
        sub_folder = f'{self.result_folder_name}/{year}/{month}'

        os.makedirs(sub_folder, exist_ok=True)
        return sub_folder

    def sorting_file(self):
        with ZipFile(file=self.folder_zip_name, mode='r') as zip_file:
            for file in zip_file.namelist():
                zip_file.extract(file)

        path = os.path.basename(self.folder_zip_name).split('.')[0]
        if os.path.isdir(path):
            for dirpath, dirnames, filenames in os.walk(path):

                if not filenames:
                    continue
                for file in filenames:
                    path_file = os.path.join(dirpath, file)
                    time_file = os.path.getmtime(path_file)
                    date = time.gmtime(time_file)

                    sub_folder = self.create_folder(year=date[0], month=date[1])
                    self.copy_file(src=path_file, dist=sub_folder)

    def copy_file(self, src, dist):
        shutil.copy2(src, dist)


class ZipSortFile(SortFile):

    def unzip_file(self):
        with ZipFile(file=self.folder_zip_name, mode='r') as zip_file:
            for file in zip_file.infolist():
                file_name = os.path.basename(file.filename)
                if not file_name:
                    continue

                sub_folder = self.create_folder(year=file.date_time[0], month=file.date_time[1])

                source = zip_file.open(file)
                target = open(os.path.join(sub_folder, file_name), "wb")
                with source, target:
                    shutil.copyfileobj(source, target)


sort_file = SortFile(file_name=path_folder_zip, result_file_name=path_folder_result)
sort_file.sorting_file()

# sort_zip_file = ZipSortFile(file_name=path_folder_zip, result_file_name=path_folder_result)
# sort_zip_file.unzip_file()

# Усложненное задание (делать по желанию)
# Нужно обрабатывать zip-файл, содержащий фотографии, без предварительного извлечения файлов в папку.
# Это относится только к чтению файлов в архиве. В случае паттерна "Шаблонный метод" изменяется способ
# получения данных (читаем os.walk() или zip.namelist и т.д.)
# Документация по zipfile: API https://docs.python.org/3/library/zipfile.html
