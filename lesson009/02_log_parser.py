# -*- coding: utf-8 -*-

# Имеется файл events.txt вида:
#
# [2018-05-17 01:55:52.665804] NOK
# [2018-05-17 01:56:23.665804] OK
# [2018-05-17 01:56:55.665804] OK
# [2018-05-17 01:57:16.665804] NOK
# [2018-05-17 01:57:58.665804] OK
# ...
#
# Напишите программу, которая считывает файл
# и выводит число событий NOK за каждую минуту в другой файл в формате
#
# [2018-05-17 01:57] 1234
# [2018-05-17 01:58] 4321
# ...
#
# Входные параметры: файл для анализа, файл результата
#
# Требования к коду: он должен быть готовым к расширению функциональности - делать сразу на классах.
# Для этого пригодится шаблон проектирование "Шаблонный метод"
#   см https://refactoring.guru/ru/design-patterns/template-method
#   и https://gitlab.skillbox.ru/vadim_shandrinov/python_base_snippets/snippets/4

path_file = 'events.txt'
out_path_file = 'out_events.txt'


class LogEvents:
    def __init__(self, file, total_file):
        self.file_name = file
        self.total_file_name = total_file
        self.file = None

    def open_file(self):
        self.file = open(file=self.file_name, mode='r')

    def close_file(self):
        self.file.close()

    def processing_file(self):
        self.open_file()
        self.read_file()
        self.close_file()

    def read_file(self):
        for line in self.file:
            edit_line = line.split(' ')
            if edit_line[2].strip() == 'NOK':
                date = edit_line[0][1:11].strip()
                time = edit_line[1][0:5].strip()

                modified_line = f'[{date} {time}]'
                self.record_file(data=modified_line)

    def record_file(self, data):
        with open(file=self.total_file_name, mode='a', encoding='utf8') as file:
            file.write(f'{data}\n')


class LogEventsTime(LogEvents):
    def read_file(self):
        for line in self.file:
            edit_line = line.split(' ')
            if edit_line[2].strip() == 'NOK':
                time = edit_line[1][0:5].strip()

                modified_line = f'[{time}]'
                self.record_file(data=modified_line)


class LogEventsMonths(LogEvents):
    def read_file(self):
        for line in self.file:
            edit_line = line.split(' ')
            if edit_line[2].strip() == 'NOK':
                date = edit_line[0][6:8].strip()

                modified_line = f'[{date}]'
                self.record_file(data=modified_line)


class LogEventsYear(LogEvents):
    def read_file(self):
        for line in self.file:
            edit_line = line.split(' ')
            if edit_line[2].strip() == 'NOK':
                date = edit_line[0][1:5].strip()

                modified_line = f'[{date}]'
                self.record_file(data=modified_line)


# log_events = LogEvents(file=path_file, total_file=out_path_file)
# log_events.processing_file()

# log_events_time = LogEventsTime(file=path_file, total_file=out_path_file)
# log_events_time.processing_file()

# log_events_time = LogEventsMonths(file=path_file, total_file=out_path_file)
# log_events_time.processing_file()

log_events_time = LogEventsYear(file=path_file, total_file=out_path_file)
log_events_time.processing_file()

# После зачета первого этапа нужно сделать группировку событий
#  - по часам
#  - по месяцу
#  - по году
