# -*- coding: utf-8 -*-

# Подсчитать статистику по буквам в романе Война и Мир.
# Входные параметры: файл для сканирования
# Статистику считать только для букв алфавита (см функцию .isalpha() для строк)
#
# Вывести на консоль упорядоченную статистику в виде
# +---------+----------+
# |  буква  | частота  |
# +---------+----------+
# |    А    |   77777  |
# |    Б    |   55555  |
# |   ...   |   .....  |
# |    a    |   33333  |
# |    б    |   11111  |
# |   ...   |   .....  |
# +---------+----------+
# |  итого  | 9999999  |
# +---------+----------+
#
# Упорядочивание по частоте - по убыванию. Ширину таблицы подберите по своему вкусу
#
# Требования к коду: он должен быть готовым к расширению функциональности - делать сразу на классах.
# Для этого пригодится шаблон проектирование "Шаблонный метод"
#   см https://refactoring.guru/ru/design-patterns/template-method
#   и https://gitlab.skillbox.ru/vadim_shandrinov/python_base_snippets/snippets/4

import zipfile

path_file_zip = 'python_snippets/voyna-i-mir.txt.zip'


class Stat:
    def __init__(self, file_name):
        self.file_name = file_name
        self.stats = {}

    def unzip(self):
        zip_file = zipfile.ZipFile(file=self.file_name, mode='r')
        for file_name in zip_file.namelist():
            zip_file.extract(file_name)
            self.file_name = file_name

    def open_file(self):
        with open(file=self.file_name, mode='r', encoding='cp1251') as file:
            for line in file:
                self.parsing(line)

    def parsing(self, line):
        for char in line:
            if char.isalpha():
                if char in self.stats:
                    self.stats[char] += 1
                else:
                    self.stats[char] = 1

    def sorting(self):
        sorted_stats = sorted(self.stats.items(), key=lambda item: item[0])
        self.stats = dict(sorted_stats)

    def print_result(self):
        print(f'+{"+":-^30}+')
        print(f'|{"буква":^14}|', f'{"частота":^14}|')
        print(f'+{"+":-^30}+')

        total_chars = 0
        for key, value in self.stats.items():
            print(f'|{key:^14}|', f'{value:^14}|')
            total_chars += value
        print(f'+{"+":-^30}+')
        print(f'|{"ИТОГО":^14}|', f'{total_chars:^14}|')
        print(f'+{"+":-^30}+')

    def processing_file(self):
        self.unzip()
        self.open_file()
        self.sorting()
        self.print_result()


class DescendingStat(Stat):
    def sorting(self):
        sorted_stats = sorted(list(self.stats.items()), key=lambda item: item[1], reverse=True)
        self.stats = dict(sorted_stats)


class AscendingStat(Stat):
    def sorting(self):
        sorted_stats = sorted(list(self.stats.items()), key=lambda item: item[1])
        self.stats = dict(sorted_stats)


# char_stat = DescendingStat(file_name=path_file_zip)
# char_stat.processing_file()

char_stat = AscendingStat(file_name=path_file_zip)
char_stat.processing_file()
