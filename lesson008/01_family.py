# -*- coding: utf-8 -*-
from pprint import pprint
from random import randint

from termcolor import cprint


# Создать модель жизни небольшой семьи.
#
# Каждый день участники жизни могут делать только одно действие.
# Все вместе они должны прожить год и не умереть.
#
# Муж может:
#   есть,
#   играть в WoT,
#   ходить на работу,
# Жена может:
#   есть,
#   покупать продукты,
#   покупать шубу,
#   убираться в доме,
#
# Все они живут в одном доме, дом характеризуется:
#   кол-во денег в тумбочке (в начале - 100)
#   кол-во еды в холодильнике (в начале - 50)
#   кол-во грязи (в начале - 0)
#
# У людей есть имя, степень сытости (в начале - 30) и степень счастья (в начале - 100).
#
# Любое действие, кроме "есть", приводит к уменьшению степени сытости на 10 пунктов
# Кушают взрослые максимум по 30 единиц еды, степень сытости растет на 1 пункт за 1 пункт еды.
# Степень сытости не должна падать ниже 0, иначе чел умрет от голода.
#
# Деньги в тумбочку добавляет муж, после работы - 150 единиц за раз.
# Еда стоит 10 денег 10 единиц еды. Шуба стоит 350 единиц.
#
# Грязь добавляется каждый день по 5 пунктов, за одну уборку жена может убирать до 100 единиц грязи.
# Если в доме грязи больше 90 - у людей падает степень счастья каждый день на 10 пунктов,
# Степень счастья растет: у мужа от игры в WoT (на 20), у жены от покупки шубы (на 60, но шуба дорогая)
# Степень счастья не должна падать ниже 10, иначе чел умирает от депрессии.
#
# Подвести итоги жизни за год: сколько было заработано денег, сколько сьедено еды, сколько куплено шуб.


class House:

    def __init__(self):
        self.money = 100
        self.food = 50
        self.dirt = 0

        self.statistic = {
            'earned_money': 0,
            'eaten': 0,
            'bought_fur_coap': 0
        }

    def __str__(self):
        return f'Денег = {self.money}, еды = {self.food}, грязи = {self.dirt}'


class Human:

    def __init__(self, name):
        self.name = name
        self.fullness = 30
        self.happiness = 100

    def __str__(self):
        return f'У {self.name}, сытость = {self.fullness}, счастье = {self.happiness}'

    def eat(self, house):
        self.fullness += 20
        house.food -= 20
        house.statistic['eaten'] += 20
        print(f'После трапезы {self.name} еды = {house.food}')

    def buy_food(self, house):
        if house.money < 80:
            print('Нет денег купить еды')
            return

        house.food += 80
        self.fullness -= 10
        house.money -= 80
        print(f'После покупки еды = {house.food}')

    def is_alive(self):
        if self.fullness <= 0:
            print(f'{self.name} умерл(а) от голода')
            return False
        elif self.happiness < 10:
            print(f'{self.name} умерл(a) от депрессии')
            return False
        else:
            return True


class Husband(Human):
    def __init__(self, name, house):
        super().__init__(name=name)
        self.house = house

    def play_game(self):
        self.happiness += 20
        self.fullness -= 10
        print(f'После игры у {self.name} счастья = {self.happiness}')

    def go_to_work(self):
        self.fullness -= 10
        self.house.money += 150
        house.statistic['earned_money'] += 350
        print(f'После похода на работу {self.name}a, денег = {self.house.money}')

    def action(self):
        self.house.dirt += 5

        if self.house.dirt > 90:
            self.happiness -= 10

        dice = randint(1, 4)

        if self.house.food < 40:
            super().buy_food(house=self.house)
        elif self.fullness <= 20:
            super().eat(house=self.house)
        elif self.happiness < 40:
            self.play_game()
        elif self.house.money < 500:
            self.go_to_work()
        elif dice == 1:
            self.go_to_work()
        elif dice == 3:
            self.play_game()
        elif dice == 4:
            super().eat(house=self.house)
        else:
            self.play_game()


class Wife(Human):

    def __init__(self, name, house):
        super().__init__(name=name)
        self.house = house

    def buy_fur_coat(self):
        if self.house.money < 350:
            print('Нет денег купить шубу')
            return
        house.statistic['bought_fur_coap'] += 1
        self.happiness += 60
        self.fullness -= 10
        self.house.money -= 350
        print(
            f'После покупки шубы, у {self.name} счастья = {self.happiness}')

    def clean(self):
        if self.house.dirt < 50:
            return
        self.fullness -= 10
        self.house.dirt -= 80
        print(f'После уборки грязи = {self.house.dirt}')

    def action(self):
        self.house.dirt += 5
        if self.house.dirt > 90:
            self.happiness -= 10

        dice = randint(1, 6)

        if self.house.food < 40:
            super().buy_food(house=self.house)
        elif self.happiness < 40:
            self.buy_fur_coat()
        elif self.fullness <= 20:
            super().eat(house=self.house)
        elif self.house.dirt > 80:
            self.clean()
        elif dice == 1:
            self.buy_fur_coat()
        elif dice == 3:
            self.clean()
        elif dice == 5:
            super().eat(house=self.house)
        elif dice == 6:
            super().buy_food(house=self.house)
        else:
            self.buy_fur_coat()


house = House()
husband = Husband(name="Федор", house=house)
wife = Wife(name='Эльвира', house=house)

for day in range(1, 366):
    print(f'========День {day}========')
    if husband.is_alive() and wife.is_alive():
        husband.action()
        wife.action()
    else:
        break

print(f'За день', husband)
print(f'За день', wife)
print(f'За день', house)
print(
    f'Было заработано денег = {house.statistic["earned_money"]}, сьедено еды = {house.statistic["eaten"]}, сколько '
    f'куплено шуб = {house.statistic["bought_fur_coap"]}')

# print(house.money)
# Подвести итоги жизни за год: сколько было заработано денег, сколько сьедено еды, сколько куплено шуб.
