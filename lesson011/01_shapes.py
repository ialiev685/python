# -*- coding: utf-8 -*-

import simple_draw as sd

sd.resolution = (1200, 600)


def factory(n):
    count_side = int(360 / n)
    color = sd.COLOR_WHITE

    def draw(dot, angle, long):
        for step_angle in range(0, 360, count_side):
            v_n = sd.get_vector(dot, angle + step_angle, long, width=3)
            v_n.draw(color=color)
            dot = v_n.end_point

    return draw


points = {
    1: sd.get_point(150, 100),
    2: sd.get_point(300, 100),
    3: sd.get_point(150, 300),
    4: sd.get_point(400, 300),
}

colors = {
    1: 'треугольник',
    2: 'квадрат',
    3: 'пятиугольника',
    4: 'шестиугольника',
}

for key, value in colors.items():
    print(key, '-', value)

number_color = input('Выберите фигуру')

if isinstance(int(number_color), int) and int(number_color) in colors:
    index = int(number_color)

    if index == 1:
        triangle = factory(3)
        triangle(dot=points[1], angle=120, long=100)
    elif index == 2:
        square = factory(4)
        square(dot=points[2], angle=90, long=100)
    elif index == 3:
        pentagon = factory(5)
        pentagon(dot=points[3], angle=72, long=100)
    elif index == 4:
        hexagon = factory(6)
        hexagon(dot=points[4], angle=60, long=100)

sd.pause()

# На основе вашего кода из решения lesson_004/01_shapes.py сделать функцию-фабрику,
# которая возвращает функции рисования треугольника, четырехугольника, пятиугольника и т.д.
#
# Функция рисования должна принимать параметры
# - точка начала рисования
# - угол наклона
# - длина стороны
#
# Функция-фабрика должна принимать параметр n - количество сторон.
