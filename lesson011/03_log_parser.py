# -*- coding: utf-8 -*-

# На основе своего кода из lesson_009/02_log_parser.py напишите итератор (или генератор)
# котрый читает исходный файл events.txt и выдает число событий NOK за каждую минуту
# <время> <число повторений>
#
# пример использования:
#
# grouped_events = <создание итератора/генератора>  # Итератор или генератор? выбирайте что вам более понятно
# for group_time, event_count in grouped_events:
#     print(f'[{group_time}] {event_count}')
#
# на консоли должно появится что-то вроде
#
# [2018-05-17 01:57] 1234

path_file = 'events.txt'


class LogEvents:
    def __init__(self, file):
        self.file_name = file
        self.file = None
        self.counter = 0
        self.split_date = ''

    def __iter__(self):
        self.open_file()
        self.i = 0
        return self

    def open_file(self):
        self.file = open(file=self.file_name, mode='r')

    def close_file(self):
        self.file.close()

    def __next__(self):
        if self.i:
            self.file.close()
            raise StopIteration()
        for line in self.file:
            if 'NOK' in line:
                date = line[1:11]
                time = line[12:17]
                self.split_date = f'{date} {time}'
                self.counter += 1
                return self.split_date, self.counter
            continue
        self.i = 1
        return self.split_date, self.counter


log_events_time = LogEvents(file=path_file)

for time, count in log_events_time:
    print(f'[{time}] {count}')

# grouped_events_gen = events_generator('events.txt')
# for group_time, event_count in grouped_events_gen:
#     print(f'[{group_time}] {event_count}')
