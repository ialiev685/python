# -*- coding: utf-8 -*-

# Написать декоратор, который будет логировать (записывать в лог файл)
# ошибки из декорируемой функции и выбрасывать их дальше.
#
# Имя файла лога - function_errors.log
# Формат лога: <имя функции> <параметры вызова> <тип ошибки> <текст ошибки>
# Лог файл открывать каждый раз при ошибке в режиме 'a'

lines = [
    'Alexandr global@gmail.com 37',
    'Marina 45',
    'Alfred afred@gmail.com 66',
    'Maxim maxim@gmail.com',
]


def log_errors_level(errors_file):
    def check_error(function):
        def write_error(*args):
            try:
                result = function(*args)
                return result
            except ValueError as exp:
                with open(file=errors_file, mode='a', encoding='utf8') as errors:
                    errors.write(f'{function.__name__} {args} {type(exp).__name__} {exp}\n')

        return write_error

    return check_error


'''Новый способ вызова функций в функции'''
@log_errors_level(errors_file='function_errors.txt')
def check_validation(*args):
    for item in args:
        name, email, age = item.split(' ')
        if not str(name):
            raise ValueError('это не имя')
        if '@' not in email or '.' not in email:
            raise ValueError('это не мэйл')
        if not int(age):
            raise ValueError('это не число возвраста')
        else:
            print(name, email, age)


''' старый способ вызова функций в функции'''
# check_error = log_errors_level(errors_file='function_errors.txt')
# write_error = check_error(check_validation)

for line in lines:
    check_validation(line)

# Проверить работу на следующих функциях
# @log_errors_level_2(errors_file='errors.txt')
# def perky(param):
#     return param / 0

# Усложненное задание (делать по желанию).
# Написать декоратор с параметром - именем файла
#
# @log_errors('function_errors.log')
# def func():
#     pass

# зачет!
