# -*- coding: utf-8 -*-

# Составить список всех живущих на районе и Вывести на консоль через запятую
# Формат вывода: На районе живут ...
# подсказка: для вывода элементов списка через запятую можно использовать функцию строки .join()
# https://docs.python.org/3/library/stdtypes.html#str.join
from district.central_street.house1 import room1 as central_street_house1_room1
from district.central_street.house1 import room2 as central_street_house1_room2
from district.central_street.house2 import room1 as central_street_house2_room1
from district.central_street.house2 import room2 as central_street_house2_room2
from district.soviet_street.house1 import room1 as soviet_street_house1_room1
from district.soviet_street.house1 import room2 as soviet_street_house1_room2
from district.soviet_street.house2 import room1 as soviet_street_house2_room1
from district.soviet_street.house2 import room2 as soviet_street_house2_room2

list_people_living_area = []

list_people_living_area.extend(central_street_house1_room1.folks)
list_people_living_area.extend(central_street_house1_room2.folks)
list_people_living_area.extend(central_street_house2_room1.folks)
list_people_living_area.extend(central_street_house2_room2.folks)
list_people_living_area.extend(soviet_street_house1_room1.folks)
list_people_living_area.extend(soviet_street_house1_room2.folks)
list_people_living_area.extend(soviet_street_house2_room1.folks)
list_people_living_area.extend(soviet_street_house2_room2.folks)

print('На районе живут:', ', '.join(list_people_living_area))
