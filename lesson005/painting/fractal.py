# -*- coding: utf-8 -*-

import simple_draw as sd


def draw_branches(point, angle, length, delta):
    if length < 10:
        return
    vector = sd.get_vector(start_point=point, angle=angle, length=length, width=3)

    vector.draw(color=sd.COLOR_YELLOW)

    next_point = vector.end_point

    next_length = length * 0.75
    draw_branches(point=next_point, angle=angle - delta, length=next_length, delta=delta)
    draw_branches(point=next_point, angle=angle + delta, length=next_length, delta=delta)
