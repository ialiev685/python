# -*- coding: utf-8 -*-


import simple_draw as sd


def make_rainbow():
    rainbow_colors = (sd.COLOR_RED, sd.COLOR_ORANGE, sd.COLOR_YELLOW, sd.COLOR_GREEN,
                      sd.COLOR_CYAN, sd.COLOR_BLUE, sd.COLOR_PURPLE)
    x1, x2 = 50, 350
    y1, y2 = 50, 450
    step = 5
    width = 4

    for color in rainbow_colors:
        print(color)
        start_point = sd.get_point(x1, y1)
        end_point = sd.get_point(x2, y2)
        sd.line(start_point, end_point, color, width)
        x1 += step
        x2 += step
