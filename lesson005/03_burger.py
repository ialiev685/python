# -*- coding: utf-8 -*-

# Создать модуль my_burger. В нем определить функции добавления инградиентов:
#  - булочки
#  - котлеты
#  - огурчика
#  - помидорчика
#  - майонеза
#  - сыра
# В каждой функции выводить на консоль что-то вроде "А теперь добавим ..."

# В этом модуле создать рецепт двойного чизбургера (https://goo.gl/zA3goZ)
# с помощью фукций из my_burger и вывести на консоль.
import my_burger



# Создать рецепт своего бургера, по вашему вкусу.
# Если не хватает инградиентов - создать соответствующие функции в модуле my_burger

burger = []

my_burger.add_bread(burger)
my_burger.add_catlet(burger)
my_burger.add_cucumber(burger)
my_burger.add_tomato(burger)
my_burger.add_mayonnaise(burger)
my_burger.add_cheese(burger)
print(burger)

# зачет!
