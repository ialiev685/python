def add_bread(burger):
    burger.append('bread')
    print('А теперь добавим булочку')
    return burger

def add_catlet(burger):
    burger.append('catlet')
    print('А теперь добавим котлету')
    return burger

def add_cucumber(burger):
    burger.append('cucumber')
    print('А теперь добавим огурец')
    return burger

def add_tomato(burger):
    burger.append('tomato')
    print('А теперь добавим помидор')
    return burger

def add_mayonnaise(burger):
    burger.append('mayonnaise')
    print('А теперь добавим майонез')
    return burger

def add_cheese(burger):
    burger.append('cheese')
    print('А теперь добавим сыр')
    return burger
