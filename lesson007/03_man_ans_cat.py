# -*- coding: utf-8 -*-

# Доработать практическую часть урока lesson_007/python_snippets/08_practice.py

# Необходимо создать класс кота. У кота есть аттрибуты - сытость и дом (в котором он живет).
# Кот живет с человеком в доме.
# Для кота дом характеризируется - миской для еды и грязью.
# Изначально в доме нет еды для кота и нет грязи.

# Доработать класс человека, добавив методы
#   подобрать кота - у кота появляется дом.
#   купить коту еды - кошачья еда в доме увеличивается на 50, деньги уменьшаются на 50.
#   убраться в доме - степень грязи в доме уменьшается на 100, сытость у человека уменьшается на 20.
# Увеличить кол-во зарабатываемых человеком денег до 150 (он выучил пайтон и устроился на хорошую работу :)

# Кот может есть, спать и драть обои - необходимо реализовать соответствующие методы.
# Когда кот спит - сытость уменьшается на 10
# Когда кот ест - сытость увеличивается на 20, кошачья еда в доме уменьшается на 10.
# Когда кот дерет обои - сытость уменьшается на 10, степень грязи в доме увеличивается на 5
# Если степень сытости < 0, кот умирает.
# Так же надо реализовать метод "действуй" для кота, в котором он принимает решение
# что будет делать сегодня

# Человеку и коту надо вместе прожить 365 дней.

# -*- coding: utf-8 -*-

from random import randint

from termcolor import cprint
from random import randint


# Реализуем модель человека.
# Человек может есть, работать, играть, ходить в магазин.
# У человека есть степень сытости, немного еды и денег.
# Если сытость < 0 единиц, человек умирает.
# Человеку надо прожить 365 дней.

class House:
    def __init__(self):
        self.dirt = 0
        self.dish = 0
        self.food = 0

    def __str__(self):
        return 'Грязь = {}, еда кота = {}, еда человека= {}'.format(self.dirt, self.dish, self.food)


class Man:

    def __init__(self, name, house):
        self.name = name
        self.money = 150
        self.cat = None
        self.house = house
        self.fullness = 150

    def eat(self):
        self.fullness += 20
        self.house.food -= 10
        cprint('{} поел, сытость = {}, еды для человека = {}'.format(self.name, self.fullness, self.house.food),
               color='yellow')

    def take_cat(self, cat):
        self.cat = cat
        self.cat.house = self.house
        self.buy_food_for_cat()
        self.cat.fullness += 20

    def buy_food_for_cat(self):
        self.house.dish += 50
        self.money -= 50
        cprint('{} купил еды для кота, еды для кота = {}, денег = {}'.format(self.name, self.house.dish, self.money),
               color='yellow')

    def buy_food_for_self(self):
        self.house.food += 50
        self.money -= 50
        cprint('Купил еды для  еды человека = {}, денег = {}'.format(self.house.dish, self.money), color='yellow')

    def clean_house(self):
        if self.house.dirt > 15:
            self.house.dirt -= 15
            self.fullness -= 40

    def go_to_work(self):
        if self.money >= 150:
            return
        self.money += 50
        cprint('{} на работу, денег = {}'.format(self.name, self.money), color='green')

    def play(self):
        self.fullness -= 10
        cprint('{} поиграл, сытость = {}'.format(self.name, self.fullness), color='yellow')

    def action(self):
        if self.fullness <= 0:
            cprint('{} умер, сытость = {}'.format(self.name, self.fullness), color='red')
            return
        if self.fullness < 20:
            self.eat()

        number_action = randint(1, 4)

        if self.house.dish < 20:
            self.buy_food_for_cat()
        elif self.house.food < 20:
            self.buy_food_for_self()
        elif number_action == 1:
            self.play()
        elif number_action == 2:
            self.eat()
        elif number_action == 3:
            self.go_to_work()
        else:
            self.play()

    def __str__(self):
        return 'Деньги = {}, сытость = {}'.format(self.money, self.fullness)


class Cat:
    def __init__(self, name):
        self.fullness = 0
        self.house = None
        self.name = name

    def sleep(self):
        if self.fullness < 0:
            self.show_dead()
        self.fullness -= 10
        cprint('{} поспал, сытость = {}'.format(self.name, self.fullness), color='blue')

    def eat(self):
        if self.house.dish <= 0:
            cprint('В доме нет еды для {}, еда = {}'.format(self.name, self.house.dish), color='red')
            return
        self.fullness += 20
        self.house.dish -= 10
        cprint('{} поел, сытость = {}, еды = {}'.format(self.name, self.fullness, self.house.dish), color='cyan')

    def destroy(self):
        self.fullness -= 10
        self.house.dirt += 5
        cprint('{} подрал обои, сытость = {}, грязь = {}'.format(self.name, self.fullness, self.house.dirt),
               color='cyan')

    def action(self):

        if self.fullness <= 0:
            cprint('{} умер, сытость = {}'.format(self.name, self.fullness), color='red')
            return
        if self.fullness < 20:
            self.eat()

        number_action = randint(1, 4)

        if number_action == 1:
            self.destroy()
        elif number_action == 2:
            self.eat()
        elif number_action == 3:
            self.sleep()
        else:
            self.sleep()

    def __str__(self):
        return f'Сытость = {self.fullness}'


cat = Cat(name='Арчибальд')
house = House()
man = Man(name='Евгений', house=house)
man.take_cat(cat=cat)
man.cat.action()

for day in range(1, 366):
    print('============День {}============'.format(day))
    man.action()
    cat.action()

print('============Конец============'.format(day))
print('ИТОГИ по человеку:', man)
print('ИТОГИ по коту:', cat)
print('ИТОГИ по дому:', house)

# Усложненное задание (делать по желанию)
# Создать несколько (2-3) котов и подселить их в дом к человеку.
# Им всем вместе так же надо прожить 365 дней.

# (Можно определить критическое количество котов, которое может прокормить человек...)

# зачет!
