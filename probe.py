def rec(number):
    if number == 1:
        return 1
    result = rec(number - 1)
    return 2 * result


print(rec(number=4))
