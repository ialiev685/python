# Есть строка с перечислением фильмов

my_favorite_movies = 'Терминатор, Пятый элемент, Аватар, Чужие, Назад в будущее'

# Выведите на консоль с помощью индексации строки, последовательно:
#   первый фильм
#   последний
#   второй
#   второй с конца

# Запятая не должна выводиться.  Переопределять my_favorite_movies нельзя
# Использовать .split() или .find()или другие методы строки нельзя - пользуйтесь только срезами,
# как указано в задании!

# Начальный и конечный нулевой индекс можно опустить [:10]

first_move = my_favorite_movies[:10]
second_move = my_favorite_movies[12:25]
third_move = my_favorite_movies[27:33]
fourth_move = my_favorite_movies[35:40]
fifth_move = my_favorite_movies[-15:]

print('первый фильм:', first_move)
print('второй фильм:', second_move)
print('третий фильм:', third_move)
print('четвертый фильм:', fourth_move)
print('пятый фильм:', fifth_move)

