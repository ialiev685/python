# в саду сорвали цветы
garden = ('ромашка', 'роза', 'одуванчик', 'ромашка', 'гладиолус', 'подсолнух', 'роза')

# на лугу сорвали цветы
meadow = ('клевер', 'одуванчик', 'ромашка', 'клевер', 'мак', 'одуванчик', 'ромашка')

# создайте множество цветов, произрастающих в саду и на лугу
# garden_set =
# meadow_set =

garden_set = set(garden)
meadow_set = set(meadow)

# выведите на консоль все виды цветов
result_1 = garden_set | meadow_set
print('все виды цветов', result_1)

# выведите на консоль те, которые растут и там и там
result_2 = garden_set & meadow_set
print('цветы которые растут и там и там', result_2)

# выведите на консоль те, которые растут в саду, но не растут на лугу
result_3 = garden_set - meadow_set
print('цветы которые растут в саду, но не растут на лугу', result_3)

# выведите на консоль те, которые растут на лугу, но не растут в саду
result_3 = meadow_set - garden_set
print('цветы которые растут на лугу, но не растут в саду', result_3)
