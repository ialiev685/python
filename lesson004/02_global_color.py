# -*- coding: utf-8 -*-
import simple_draw as sd


# Добавить цвет в функции рисования геом. фигур. из упр lesson_004/shapes.py
# (код функций скопировать сюда и изменить)
# Запросить у пользователя цвет фигуры посредством выбора из существующих:
#   вывести список всех цветов с номерами и ждать ввода номера желаемого цвета.
# Потом нарисовать все фигуры этим цветом

# Пригодятся функции
# sd.get_point()
# sd.line()
# sd.get_vector()
# и константы COLOR_RED, COLOR_ORANGE, COLOR_YELLOW, COLOR_GREEN, COLOR_CYAN, COLOR_BLUE, COLOR_PURPLE
# Результат решения см lesson_004/results/exercise_02_global_color.jpg

def draw(dot, angle, long, color):
    end_point = dot
    for n in range(0, 360, angle):
        v_n = sd.get_vector(dot, angle + n, long, width=3)
        v_n.draw(color=color)
        dot = v_n.end_point


colors = [
    {'color': sd.COLOR_RED, 'title': 'красный'},
    {'color': sd.COLOR_ORANGE, 'title': 'оранжевый'},
    {'color': sd.COLOR_YELLOW, 'title': 'желтый'},
    {'color': sd.COLOR_GREEN, 'title': 'зеленный'},
    {'color': sd.COLOR_CYAN, 'title': 'сине-зеленый'},
    {'color': sd.COLOR_BLUE, 'title': 'голубой'},
    {'color': sd.COLOR_PURPLE, 'title': 'фиолетовый'},
]

for index, item in enumerate(colors):
    print(index, '-', item['title'])

number_color = input('Выберите цвет')
default_color = sd.COLOR_WHITE

if isinstance(int(number_color), int) and 7 > int(number_color) >= 0:
    index = int(number_color)
    default_color = colors[index]['color']


def triangle(dot, angle, long, color):
    draw(dot, angle, long, color)


point_0 = sd.get_point(150, 100)
triangle(dot=point_0, angle=120, long=100, color=default_color)


def square(dot, angle, long, color):
    draw(dot, angle, long, color)


point_1 = sd.get_point(300, 100)
square(dot=point_1, angle=90, long=100, color=default_color)


def pentagon(dot, angle, long, color):
    draw(dot, angle, long, color)


point_2 = sd.get_point(150, 300)
square(dot=point_2, angle=72, long=100, color=default_color)


def hexagon(dot, angle, long, color):
    draw(dot, angle, long, color)


point_3 = sd.get_point(400, 300)
square(dot=point_3, angle=60, long=100, color=default_color)

sd.pause()
