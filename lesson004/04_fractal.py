# -*- coding: utf-8 -*-

import simple_draw as sd

sd.resolution = (1200, 600)


# 1) Написать функцию draw_branches, которая должна рисовать две ветви дерева из начальной точки
# Функция должна принимать параметры:
# - точка начала рисования,
# - угол рисования,
# - длина ветвей,
# Отклонение ветвей от угла рисования принять 30 градусов,

def draw_branches(point, angle, length, delta):
    if length < 10:
        return
    vector = sd.get_vector(start_point=point, angle=angle, length=length, width=3)

    vector.draw(color=sd.COLOR_YELLOW)

    next_point = vector.end_point

    next_length = length * 0.75
    draw_branches(point=next_point, angle=angle-delta, length=next_length,delta=delta)
    draw_branches(point=next_point, angle=angle+delta, length=next_length,delta=delta)


# point_0 = sd.get_point(400, 100)
# angle = 90
# length = 100
# next_angle = angle - 30
# draw_branches(point=point_0, angle=next_angle, length=length)
# next_angle = angle + 30
# draw_branches(point=point_0, angle=next_angle, length=length)

# 2) Сделать draw_branches рекурсивной
# - добавить проверку на длину ветвей, если длина меньше 10 - не рисовать
# - вызывать саму себя 2 раза из точек-концов нарисованных ветвей,
#   с параметром "угол рисования" равным углу только что нарисованной ветви,
#   и параметром "длина ветвей" в 0.75 меньшей чем длина только что нарисованной ветви

point_0 = sd.get_point(400, 100)
angle = 90
length = 200

draw_branches(point=point_0, angle=angle, length=length, delta=30)

# 3) Запустить вашу рекурсивную функцию, используя следующие параметры:
root_point = sd.get_point(300, 30)
# draw_branches(point=root_point, angle=90, length=100,delta=10)

# Пригодятся функции
# sd.get_point()
# sd.get_vector()
# Возможный результат решения см lesson_004/results/exercise_04_fractal_01.jpg

# можно поиграть -шрифтами- цветами и углами отклонения

# def draw_branches(start_point, angle, length, delta):
#     if length < 10:
#         return
#     vector = sd.get_vector(start_point=start_point, angle=angle, length=length)
#     vector.draw()
#     length *= .75
#     draw_branches(start_point=vector.end_point, angle=angle-delta, length=length, delta=delta)
#     draw_branches(start_point=vector.end_point, angle=angle+delta, length=length, delta=delta)

# 4) Усложненное задание (делать по желанию)
# - сделать рандомное отклонение угла ветвей в пределах 40% от 30-ти градусов
# - сделать рандомное отклонение длины ветвей в пределах 20% от коэффициента 0.75
# Возможный результат решения см lesson_004/results/exercise_04_fractal_02.jpg

# Пригодятся функции
# sd.random_number()
sd.pause()
