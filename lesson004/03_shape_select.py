# -*- coding: utf-8 -*-

import simple_draw as sd


# Запросить у пользователя желаемую фигуру посредством выбора из существующих
#   вывести список всех фигур с номерами и ждать ввода номера желаемой фигуры.
# и нарисовать эту фигуру в центре экрана

# Код функций из упр lesson_004/02_global_color.py скопировать сюда
# Результат решения см lesson_004/results/exercise_03_shape_select.jpg


def draw(dot, angle, long, color):
    end_point = dot
    for n in range(0, 360, angle):
        v_n = sd.get_vector(dot, angle + n, long, width=3)
        v_n.draw(color=color)
        dot = v_n.end_point


def triangle(dot, angle, long, color):
    draw(dot, angle, long, color)


def square(dot, angle, long, color):
    draw(dot, angle, long, color)


def pentagon(dot, angle, long, color):
    draw(dot, angle, long, color)


def hexagon(dot, angle, long, color):
    draw(dot, angle, long, color)


default_color = sd.COLOR_WHITE

points = {
    1: sd.get_point(150, 100),
    2: sd.get_point(300, 100),
    3: sd.get_point(150, 300),
    4: sd.get_point(400, 300),
}

colors = {
    1: 'треугольник',
    2: 'квадрат',
    3: 'пятиугольника',
    4: 'шестиугольника',
}

for key, value in colors.items():
    print(key, '-', value)

number_color = input('Выберите фигуру')

if isinstance(int(number_color), int) and int(number_color) in colors:
    index = int(number_color)

    if index == 1:
        triangle(dot=points[1], angle=120, long=100, color=default_color)
    elif index == 2:
        square(dot=points[2], angle=90, long=100, color=default_color)
    elif index == 3:
        pentagon(dot=points[3], angle=72, long=100, color=default_color)
    elif index == 4:
        hexagon(dot=points[4], angle=60, long=100, color=default_color)

sd.pause()
