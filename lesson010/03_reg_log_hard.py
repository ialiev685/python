# -*- coding: utf-8 -*-

# Есть файл с протоколом регистраций пользователей на сайте - registrations.txt
# Каждая строка содержит: ИМЯ ЕМЕЙЛ ВОЗРАСТ, разделенные пробелами
# Например:
# Василий test@test.ru 27
#
# Надо проверить данные из файла, для каждой строки:
# - присутсвуют все три поля
# - поле имени содержит только буквы
# - поле емейл содержит @ и .
# - поле возраст является числом от 10 до 99
#
# В результате проверки нужно сформировать два файла
# - registrations_good.log для правильных данных, записывать строки как есть
# - registrations_bad.log для ошибочных, записывать строку и вид ошибки.
#
# Для валидации строки данных написать метод, который может выкидывать исключения:
# - НЕ присутсвуют все три поля: ValueError
# - поле имени содержит НЕ только буквы: NotNameError (кастомное исключение)
# - поле емейл НЕ содержит @ и .(точку): NotEmailError (кастомное исключение)
# - поле возраст НЕ является числом от 10 до 99: ValueError
# Вызов метода обернуть в try-except.

file_path_import_data = 'registrations.txt'
file_path_valid_data = './registrations_good.log'
file_path_invalid_data = './registrations_bad.log'


class NotNameError(Exception):

    def __str__(self):
        return 'поле имени содержит НЕ только буквы'


class NotEmailError(Exception):
    def __str__(self):
        return 'поле емейл НЕ содержит @ и .(точку)'


class FileRead:
    def __init__(self, file_name_import, file_name_good, file_name_bad):
        self.file_name_import = file_name_import
        self.file_name_good = file_name_good
        self.file_name_bad = file_name_bad

    def read_file(self):
        with open(file=self.file_name_import, mode='r', encoding='utf8') as file:
            for line in file:
                self.parse_line_file(line)

    def parse_line_file(self, line_file):
        try:
            if len(line_file.split(' ')) < 3:
                raise ValueError('НЕ присутствуют все три поля')

            name, email, age = line_file.split(' ')

            if not isinstance(str(name), str):
                raise NotNameError
            if '@' not in email or '.' not in email:
                raise NotEmailError
            if not isinstance(int(age), int) or 99 < int(age) < 10:
                raise ValueError('поле возраст НЕ является числом от 10 до 99')
            self.write_valid_line_file(line_file)

        except ValueError as exc:
            write_line_file = f'{line_file.rstrip()} - {exc}'
            self.write_invalid_line_file(line_file=write_line_file)
        except NotNameError as exc:
            write_line_file = f'{line_file.rstrip()} - {exc}'
            self.write_invalid_line_file(line_file=write_line_file)
        except NotEmailError as exc:
            write_line_file = f'{line_file.rstrip()} - {exc}'
            self.write_invalid_line_file(line_file=write_line_file)

    def write_valid_line_file(self, line_file):
        with open(self.file_name_good, mode='a', encoding='utf8') as file:
            file.write(f'{line_file} \n')

    def write_invalid_line_file(self, line_file):
        with open(self.file_name_bad, mode='a', encoding='utf8') as file:
            file.write(f'{line_file} \n')


file_read = FileRead(file_name_import=file_path_import_data, file_name_good=file_path_valid_data,
                     file_name_bad=file_path_invalid_data)
file_read.read_file()
