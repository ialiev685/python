# -*- coding: utf-8 -*-

# День сурка
#
# Напишите функцию one_day() которая возвращает количество кармы от 1 до 7
# и может выкидывать исключения:
# - IamGodError
# - DrunkError
# - CarCrashError
# - GluttonyError
# - DepressionError
# - SuicideError
# Одно из этих исключений выбрасывается с вероятностью 1 к 13 каждый день
#
# Функцию оберните в бесконечный цикл, выход из которого возможен только при накоплении
# кармы до уровня ENLIGHTENMENT_CARMA_LEVEL. Исключения обработать и записать в лог.
# При создании собственных исключений максимально использовать функциональность
# базовых встроенных исключений.
import os
from random import randint, choice

ENLIGHTENMENT_CARMA_LEVEL = 777


class IamGodError(Exception):
    def __str__(self):
        return 'Я бог'


class DrunkError(Exception):
    def __str__(self):
        return 'Я напился'


class CarCrashError(Exception):
    def __str__(self):
        return 'Я разбился'


class GluttonyError(Exception):
    def __str__(self):
        return 'Я обожался'


class DepressionError(Exception):
    def __str__(self):
        return 'Я в депрессии'


class SuicideError(Exception):
    def __str__(self):
        return 'Я покончил жизнь самоубийством'


errors = [
    IamGodError,
    DrunkError,
    CarCrashError,
    GluttonyError,
    DepressionError,
    SuicideError,
]

carma = 0
day = 0
logs = []


def one_day():
    if randint(1, 13) == 13:
        exp = choice(errors)
        raise exp()
    return randint(1, 7)


while carma < ENLIGHTENMENT_CARMA_LEVEL:
    day += 1
    try:
        carma += one_day()
    except IamGodError as exc:
        print(f'{exc}')
        logs.append(f'{exc}')
    except DrunkError as exc:
        print(f'{exc}')
        logs.append(f'{exc}')
    except CarCrashError as exc:
        print(f'{exc}')
        logs.append(f'{exc}')
    except GluttonyError as exc:
        print(f'{exc}')
        logs.append(f'{exc}')
    except DepressionError as exc:
        print(f'{exc}')
        logs.append(f'{exc}')
    except SuicideError as exc:
        print(f'{exc}')
        logs.append(f'{exc}')
    finally:
        print('Прошел день:', day)

print('карма достигла:', carma)
print('logs:', logs)

with open('carma.txt', 'w', encoding='utf8') as file:
    for line in logs:
        line = line + '\n'
        file.write(line)
    file.write(f'Мне понадобилось {day} дней чтобы достичь кармы')

# https://goo.gl/JnsDqu

# зачет!
