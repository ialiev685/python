# -*- coding: utf-8 -*-

# (if/elif/else)

# По номеру месяца вывести кол-во дней в нем (без указания названия месяца, в феврале 28 дней)
# Результат проверки вывести на консоль
# Если номер месяца некорректен - сообщить об этом

# объекты данных всегда объявляем до основной логики программы

month = {
    1: {'month': 'январь', 'day': 31},
    2: {'month': 'февраль', 'day': 28},
    3: {'month': 'март', 'day': 31},
    4: {'month': 'апрель', 'day': 30},
    5: {'month': 'май', 'day': 31},
    6: {'month': 'июнь', 'day': 30},
    7: {'month': 'июль', 'day': 31},
    8: {'month': 'август', 'day': 31},
    9: {'month': 'сентябь', 'day': 30},
    10: {'month': 'октябрь', 'day': 31},
    11: {'month': 'ноябрь', 'day': 30},
    12: {'month': 'декабрь', 'day': 31},
}

# Номер месяца получать от пользователя следующим образом
user_input = input('Введите, пожалуйста, номер месяца: ')
# вторую проверку можно сделать вот такую


if user_input.isdigit() and int(user_input) in month:
    key = int(user_input)
    print(month[key]['month'], month[key]['day'])
else:
    print('Введите число в диапозоне от 1 до 12')
