# -*- coding: utf-8 -*-

# (цикл for)
import simple_draw as sd

# Нарисовать стену из кирпичей. Размер кирпича - 100х50
# Использовать вложенные циклы for

# Подсказки:
#  Для отрисовки кирпича использовать функцию rectangle
#  Алгоритм должен получиться приблизительно такой:
#
#   цикл по координате Y
#       вычисляем сдвиг ряда кирпичей
#       цикл координате X
#           вычисляем правый нижний и левый верхний углы кирпича
#           рисуем кирпич

brick_x, brick_y = 100, 50
color = sd.COLOR_WHITE

for counter, y in enumerate(range(0, 500, 50)):
    shift = 0 if counter % 2 == 0 else 50
    for x in range(shift, 2000, 100):
        start_point = sd.get_point(x, y)
        end_point = sd.get_point(x + brick_x, y + brick_y)
        sd.rectangle(start_point, end_point, color, width=1)

sd.pause()
