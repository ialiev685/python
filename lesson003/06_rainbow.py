# -*- coding: utf-8 -*-

# (цикл for)

import simple_draw as sd

rainbow_colors = (sd.COLOR_RED, sd.COLOR_ORANGE, sd.COLOR_YELLOW, sd.COLOR_GREEN,
                  sd.COLOR_CYAN, sd.COLOR_BLUE, sd.COLOR_PURPLE)

# Нарисовать радугу: 7 линий разного цвета толщиной 4 с шагом 5 из точки (50, 50) в точку (350, 450)
# Подсказка: цикл нужно делать сразу по тьюплу с цветами радуги.

x1, x2 = 50, 350
y1, y2 = 50, 450
step = 5
width = 4

for color in rainbow_colors:
    print(color)
    start_point = sd.get_point(x1, y1)
    end_point =  sd.get_point(x2, y2)
    sd.line(start_point, end_point, color,width)
    x1 += step
    x2 += step

sd.pause()

# Усложненное задание, делать по желанию.
# Нарисовать радугу дугами от окружности (cсм sd.circle) за нижним краем экрана,
# поэкспериментировать с параметрами, что бы было красиво


