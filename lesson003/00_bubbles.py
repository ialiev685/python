# -*- coding: utf-8 -*-

import simple_draw as sd

sd.resolution = (1400, 600)

# Нарисовать пузырек - три вложенных окружностей с шагом 5 пикселей
position = sd.get_point(100, 100)
radius = 50

for _ in range(0, 3):
    sd.circle(position, radius)
    radius += 5


# Написать функцию рисования пузырька, принммающую 3 (или более) параметра: точка рисования, шаг и цвет
def draw(position, radius, color):
    for _ in range(3):
        sd.circle(position, radius, color)
        radius += 5


# Нарисовать 10 пузырьков в ряд
radius_10_bold = 25
color_yellow = sd.COLOR_ORANGE

for x in range(50, 550, 50):
    position_10_bold = sd.get_point(x, 200)
    draw(position_10_bold, radius_10_bold, color_yellow)

# Нарисовать три ряда по 10 пузырьков
radius_3_row_bold = 25
color_purple = sd.COLOR_PURPLE

for y in range(400, 550, 50):
    for x in range(50, 550, 50):
        position_3_row = sd.get_point(x, y)
        draw(position_3_row, radius_3_row_bold, color_purple)

# Нарисовать 100 пузырьков в произвольных местах экрана случайными цветами
radius_100_bold = 50
color_red = sd.COLOR_RED

for y in range(100, 10000, 100):
    random_x = sd.random_number(100, 1300)
    random_y = sd.random_number(100, 1300)
    position_100_bold = sd.get_point(random_x, random_y)
    draw(position_100_bold, radius_100_bold, color_red)

sd.pause()
