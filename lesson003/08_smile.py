# -*- coding: utf-8 -*-

# (определение функций)
import simple_draw as sd


# Написать функцию отрисовки смайлика по заданным координатам
# Форма рожицы-смайлика на ваше усмотрение
# Параметры функции: кордината X, координата Y, цвет.
# Вывести 10 смайликов в произвольных точках экрана.


def make_smile(x, y, color_face):
    start_point_face = sd.get_point(x, y)
    end_point_face = sd.get_point(x + 100, y + 80)
    sd.ellipse(start_point_face, end_point_face, color_face)

    color_eye = sd.COLOR_DARK_RED
    start_point_eye = sd.get_point(x + 20, y + 40)
    end_point_eye = sd.get_point(x + 40, y + 60)
    sd.ellipse(start_point_eye, end_point_eye, color_eye)
    start_point_eye = sd.get_point(x + 60, y + 40)
    end_point_eye = sd.get_point(x + 80, y + 60)
    sd.ellipse(start_point_eye, end_point_eye, color_eye)

    color_mouth = sd.COLOR_DARK_RED
    start_point_mouth = sd.get_point(x + 20, y + 30)
    middle_point_mouth1 = sd.get_point(x + 40, y + 20)
    middle_point_mouth2 = sd.get_point(x + 60, y + 20)
    end_point_mouth = sd.get_point(x + 80, y + 30)
    sd.lines([start_point_mouth,middle_point_mouth1,middle_point_mouth2,end_point_mouth],color_mouth,width=1)




color = sd.COLOR_ORANGE

for _ in range(0, 10):
    x = sd.random_number(1, 500)
    y = sd.random_number(1, 300)
    make_smile(x, y, color)

sd.pause()
